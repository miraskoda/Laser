﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Threading;
using System.IO.Ports;
using System.Windows;
using System.Windows.Forms;
using Un4seen.Bass;
using Un4seen.BassWasapi;
using System.Windows.Forms.DataVisualization.Charting;
using System.Windows.Navigation;

namespace LaserControl
{
    public class Audio
    {
        public DispatcherTimer _t; //timer that refreshes the display
        public float[] _fft; //buffer for fft data
        private ProgressBar _l, _r; //progressbars for left and right channel intensity
        private WASAPIPROC _process; //callback function to obtain data
        private int _lastlevel; //last output level
        private int _hanctr; //last output level counter
        private static ComboBox _devicelist; //device list
        public bool _initialized; //initialized flag
        private int devindex; //used device index

        //constructor
        public Audio(ProgressBar left, ProgressBar right, ComboBox devicelist)

    {
            _fft = new float[8192];
            _lastlevel = 0;
            _hanctr = 0;
            _t = new DispatcherTimer();
            _t.Tick += _t_Tick;
            _t.Interval = TimeSpan.FromMilliseconds(25); //40hz refresh rate//25
            _t.IsEnabled = false;
            _l = left;
            _r = right;
            _l.Minimum = 0;
            _r.Minimum = 0;
            _r.Maximum = (ushort.MaxValue);
            _l.Maximum = (ushort.MaxValue);
            _process = new WASAPIPROC(Process);
            _devicelist = devicelist;
        _initialized = false;
            Init();
    }

        //init device
        public void Initialization()
        {           
             var array = (_devicelist.Items[_devicelist.SelectedIndex] as string)?.Split(' ');
             devindex = Convert.ToInt32(array?[0]);
             BassWasapi.BASS_WASAPI_Init(devindex, 0, 0, BASSWASAPIInit.BASS_WASAPI_BUFFER, 1f, 0.05f, _process, IntPtr.Zero);
             _t.IsEnabled = true; //timer redraw enabled
             BassWasapi.BASS_WASAPI_Start();
        }

        // initialization
        public void Init()
        {
            LoadDevice();
            Bass.BASS_SetConfig(BASSConfig.BASS_CONFIG_UPDATETHREADS, false);
            Bass.BASS_Init(0, 44100, BASSInit.BASS_DEVICE_DEFAULT, IntPtr.Zero);
        }

        public static ComboBox LoadDevice()
        {

            for (int i = 0; i < BassWasapi.BASS_WASAPI_GetDeviceCount(); i++)
            {
                var device = BassWasapi.BASS_WASAPI_GetDeviceInfo(i);
                if (device.IsEnabled && device.IsLoopback)
                {
                    _devicelist.Items.Add(string.Format("{0} - {1}", i, device.name));
                }
            }
            BassWasapi.BASS_WASAPI_Start();
            _devicelist.SelectedIndex = 0;

            return (_devicelist);
        }

        //timer 
        private void _t_Tick(object sender, EventArgs e)
        {
            int ret = BassWasapi.BASS_WASAPI_GetData(_fft, (int)BASSData.BASS_DATA_FFT8192);  //get ch.annel fft data
            if (ret < -1) return;
            int x, y;
            int b0 = 0;
   
            int level = BassWasapi.BASS_WASAPI_GetLevel()*3;         
            _l.Value = (Utils.LowWord32(level));
            _r.Value = (Utils.HighWord32(level));
            if (level == _lastlevel && level != 0) _hanctr++;
            _lastlevel = level;

            //Required, because some programs hang the output. If the output hangs for a 75ms
            //this piece of code re initializes the output so it doesn't make a gliched sound for long.
            if (_hanctr > 3)
            {
                _hanctr = 0;
                _l.Value = (0);
                _r.Value = (0);
                Free();
                Bass.BASS_Init(0, 44100, BASSInit.BASS_DEVICE_DEFAULT, IntPtr.Zero);
                Initialization();
                
            }

        }
        
        //nepotrebny nutny sracky 
        // WASAPI callback, required for continuous recording
        private int Process(IntPtr buffer, int length, IntPtr user)
        {
            return length;
        }
        //cleanup
        public void Free()
        {
            BassWasapi.BASS_WASAPI_Free();
            Bass.BASS_Free();
        }
    }
}
